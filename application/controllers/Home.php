<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Home extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->user = $this->M_user->getBy(['username' => $this->session->userdata['username']])->row_array();
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Dashboard', site_url('home'));

        $data['title'] = 'Dashboard';
        $data['profile'] = 'My Profile';
        $data['user'] = $this->user;
        $data['users'] = $this->M_user->get()->num_rows();

        $this->template->load('template', 'home/beranda', $data);
    }
}
