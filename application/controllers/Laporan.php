<?php

class Laporan extends CI_Controller
{
    public $user;

    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }
        $this->load->model('M_user');
        $this->load->model('M_pendapatan', 'pendapatan');
        $this->user = $this->M_user->getBy(['username' => $this->session->userdata['username']])->row_array();
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Pendapatan', site_url('home'));

        if (!empty($_POST)) {
            $tglmulai = $this->input->post('tglmulai');
            $tglsampai = $this->input->post('tglsampai');
            $data['pendapatan'] = $this->pendapatan->getPendapatan($tglmulai, $tglsampai)->result_array();
        } else {
            $data['pendapatan'] = null;
        }
        $data['title'] = 'Pendapatan';
        $data['profile'] = 'Pendapatan';
        $data['active'] = 'Laporan';
        $data['user'] = $this->user;
        $data['users'] = $this->M_user->get()->num_rows();

        $this->template->load('template', 'laporan/index', $data);
    }
}
