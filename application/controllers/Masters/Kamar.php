<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kamar extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_kelaskamar', 'kelaskamar');
        $this->load->model('M_statuskamar', 'status');

        //set default
        $this->title = 'Data Kamar';
        $this->menu = 'kamar';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_data = $this->kelaskamar->getListCombo();
        $a_status = $this->status->getListCombo();
        $a_color = [
            '1' => 'success',
            '2' => 'danger',
            '3' => 'warning'
        ];

        $a_kolom = [];
        $a_kolom[] = ['kolom' => 'idkamar', 'label' => 'ID Kamar'];
        $a_kolom[] = ['kolom' => 'namakamar', 'label' => 'Nama Kamar'];
        $a_kolom[] = ['kolom' => 'idkelaskamar', 'label' => 'Tipe Kamar', 'type' => 'S', 'option' => $a_data];
        $a_kolom[] = ['kolom' => 'kapasitas', 'label' => 'Kapasitas', 'type' => 'N'];
        $a_kolom[] = ['kolom' => 'harga', 'label' => 'Harga', 'type' => 'N', 'set_currency' => true];
        $a_kolom[] = ['kolom' => 'foto', 'label' => 'Foto', 'type' => 'F', 'path' => './assets/img/kamar/', 'file_type' => 'jpg|jpeg|png', 'is_tampil' => false];
        $a_kolom[] = ['kolom' => 'fasilitas', 'label' => 'Fasilitas', 'type' => 'A', 'is_tampil' => false];
        $a_kolom[] = ['kolom' => 'deskripsi', 'label' => 'Deskripsi', 'type' => 'A', 'is_tampil' => false, 'is_null' => true];
        $a_kolom[] = ['kolom' => 'info_tambahan', 'label' => 'Info Tambahan', 'type' => 'A', 'is_tampil' => false, 'is_null' => true];
        $a_kolom[] = ['kolom' => 'idstatus', 'label' => 'Status', 'type' => 'S', 'option' => $a_status, 'view_type' => 'badge', 'color' => $a_color];

        $this->a_kolom = $a_kolom;
    }
}
