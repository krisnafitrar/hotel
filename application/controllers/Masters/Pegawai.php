<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pegawai extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_posisi', 'posisi');

        //set default
        $this->title = 'Data Pegawai';
        $this->menu = 'pegawai';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_data = $this->posisi->getListCombo();
        $a_status = ['1' => 'Aktif', '2' => 'Non-Aktif'];
        $a_jk = [
            'L' => 'Laki-Laki',
            'P' => 'Perempuan'
        ];

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'noidentitas', 'label' => 'No Indentitas'];
        $a_kolom[] = ['kolom' => 'namapegawai', 'label' => 'Nama Pegawai'];
        $a_kolom[] = ['kolom' => 'jeniskelamin', 'label' => 'Jenis Kelamin', 'type' => 'S', 'option' => $a_jk];
        $a_kolom[] = ['kolom' => 'notelp', 'label' => 'No Telp', 'type' => 'N'];
        $a_kolom[] = ['kolom' => 'idposisi', 'label' => 'Posisi', 'type' => 'S', 'option' => $a_data];
        $a_kolom[] = ['kolom' => 'foto', 'label' => 'Foto', 'type' => 'F', 'path' => './assets/img/pegawai/', 'file_type' => 'jpg|jpeg|png'];
        $a_kolom[] = ['kolom' => 'alamat', 'label' => 'Alamat', 'type' => 'A', 'is_tampil' => false];
        $a_kolom[] = ['kolom' => 'status', 'label' => 'Status', 'type' => 'S', 'option' => $a_status];

        $this->a_kolom = $a_kolom;
    }
}
