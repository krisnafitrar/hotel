<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Posisi extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        //set default
        $this->title = 'List Posisi';
        $this->menu = 'posisi';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'posisi', 'label' => 'Posisi'];
        $a_kolom[] = ['kolom' => 'info_tambahan', 'label' => 'Info Tambahan', 'type' => 'A'];

        $this->a_kolom = $a_kolom;
    }
}
