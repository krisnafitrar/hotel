<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Tamu extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        //set default
        $this->title = 'Data Tamu';
        $this->menu = 'tamu';
        $this->parent = 'masters';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {

        $a_data = [
            'L' => 'Laki-Laki',
            'P' => 'Perempuan'
        ];

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'namatamu', 'label' => 'Nama tamu'];
        $a_kolom[] = ['kolom' => 'jeniskelamin', 'label' => 'Jenis Kelamin', 'type' => 'S', 'option' => $a_data];
        $a_kolom[] = ['kolom' => 'notelp', 'label' => 'No telp', 'type' => 'N'];
        $a_kolom[] = ['kolom' => 'alamat', 'label' => 'Alamat', 'type' => 'A'];
        $a_kolom[] = ['kolom' => 'info_tambahan', 'label' => 'Info Tambahan', 'type' => 'A'];

        $this->a_kolom = $a_kolom;
    }
}
