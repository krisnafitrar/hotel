<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Menu extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        //set default
        $this->title = 'Pengaturan Menu';
        $this->menu = 'menu';
        $this->parent = 'pengaturan';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_status = [
            '1' => 'Ya',
            '0' => 'Tidak'
        ];

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'menu', 'label' => 'Menu'];
        $a_kolom[] = ['kolom' => 'icon', 'label' => 'Icon'];
        $a_kolom[] = ['kolom' => 'is_active', 'label' => 'Aktif', 'type' => 'S', 'option' => $a_status];

        $this->a_kolom = $a_kolom;
    }
}
