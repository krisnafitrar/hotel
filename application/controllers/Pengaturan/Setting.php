<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Setting extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        //set default
        $this->title = 'Pengaturan Aplikasi';
        $this->menu = 'setting';
        $this->parent = 'pengaturan';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_kolom = [];
        $a_kolom[] = ['kolom' => 'idpengaturan', 'label' => 'Kode Pengaturan'];
        $a_kolom[] = ['kolom' => 'namapengaturan', 'label' => 'Nama Pengaturan'];
        $a_kolom[] = ['kolom' => 'value', 'label' => 'Value', 'type' => 'A'];

        $this->a_kolom = $a_kolom;
    }
}
