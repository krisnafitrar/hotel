<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Submenu extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_menu');

        //set default
        $this->title = 'Pengaturan Submenu';
        $this->menu = 'submenu';
        $this->parent = 'pengaturan';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_status = [
            '1' => 'Ya',
            '0' => 'Tidak'
        ];

        $a_menu = $this->M_menu->getListCombo();

        $a_kolom = [];
        $a_kolom[] = ['kolom' => ':no', 'label' => 'No', 'is_null' => true];
        $a_kolom[] = ['kolom' => 'submenu', 'label' => 'Submenu'];
        $a_kolom[] = ['kolom' => 'idmenu', 'label' => 'Menu', 'type' => 'S', 'option' => $a_menu];
        $a_kolom[] = ['kolom' => 'url', 'label' => 'URL'];
        $a_kolom[] = ['kolom' => 'is_active', 'label' => 'Aktif', 'type' => 'S', 'option' => $a_status];

        $this->a_kolom = $a_kolom;
    }
}
