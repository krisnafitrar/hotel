<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Checkin extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_kamar');
        $this->load->model('M_tamu', 'tamu');
        $this->load->model('M_transaksi', 'transaksi');
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Check In', site_url('transaksi/checkin'));

        $data['title'] = 'Check In';
        $data['profile'] = 'Kamar Tersedia';
        $data['user'] = $this->user;
        $data['users'] = $this->M_user->get()->num_rows();
        if ($this->input->post('submit')) {
            $keyword = $this->input->post('keyword');
            $data['kamar'] = $this->M_kamar->getByKeyword($keyword, 1)->result_array();
        } else {
            $data['kamar'] = $this->M_kamar->getKamar(1)->result_array();
        }
        $this->template->load('template', 'transaksi/index', $data);

        if ($_POST) {
            $invoice = 'INV' . time();
            $checkin = $this->input->post('checkin');
            $waktu_checkin = $this->input->post('waktu_checkin');
            $checkout = $this->input->post('checkout');
            $waktu_checkout = $this->input->post('waktu_checkout');
            $tamu = $this->input->post('tamu');
            $total = $this->input->post('total');
            $deposit = $this->input->post('deposit');
            $idkamar = $this->input->post('idkamar');
            $time = time();

            $tgl1 = new DateTime($checkin);
            $tgl2 = new DateTime($checkout);
            $d = $tgl2->diff($tgl1)->days;

            $a_data = [
                'invoice' => $invoice,
                'tglcheckin' => $checkin,
                'waktucheckin' => $waktu_checkin,
                'tglcheckout' => $checkout,
                'waktucheckout' => $waktu_checkout,
                'durasi' => $d,
                'idtamu' => $tamu,
                'idkamar' => $idkamar,
                'total' => $total * $d,
                'deposit' => $deposit,
                'time' => $time
            ];

            $a_status = [
                'idstatus' => 2
            ];

            if ($deposit >= ($total * $d / 2)) {
                $this->transaksi->beginTrans();
                $this->transaksi->insert($a_data);
                $this->M_kamar->update($a_status, $idkamar);
                $ok = $this->transaksi->statusTrans();
                $this->transaksi->commitTrans($ok);

                $ok && $ok ? setMessage('Berhasil melakukan check in', 'success') : setMessage('Gagal melakukan check in', 'danger');
                redirect('transaksi/checkin');
            } else {
                setMessage('Deposit tidak cukup!', 'danger');
                redirect('transaksi/checkin/detail/' . $idkamar);
            }
        }
    }

    public function detail($id)
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Check In', site_url('transaksi/checkin'));
        $this->breadcrumb->append_crumb('Detail', site_url('transaksi/checkin/detail/' . $id));


        $data['title'] = 'Detail';
        $data['profile'] = 'Check In Sekarang';
        $data['user'] = $this->user;
        $data['users'] = $this->M_user->get()->num_rows();
        $data['kamar'] = $this->db->get_where('kamar', ['idkamar' => $id])->row_array();
        $this->template->load('template', 'transaksi/detail_checkin', $data);
    }

    public function cariTamu()
    {
        $keyword = $this->input->post('cari');
        $query = "lower(idtamu) like '%" . strtolower($keyword) . "%' or lower(namatamu) like '%" . strtolower($keyword) . "%'";
        $tamu = $this->tamu->getBy($query);

        if ($tamu->num_rows() < 1) {
            echo json_encode(array());
        } else {
            $data = array();
            foreach ($tamu->result_array() as $val) {
                $data[] = array('id' => $val['idtamu'], 'text' => $val['idtamu'] . ' - ' . $val['namatamu']);
            }
            echo json_encode($data);
        }
    }
}
