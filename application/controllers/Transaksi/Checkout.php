<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Checkout extends MY_Controller
{
    public $user;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('M_kamar');
        $this->load->model('M_transaksi');
        $this->load->model('M_pendapatan');
        $this->user = $this->M_user->getBy(['username' => $this->session->userdata['username']])->row_array();
    }

    public function index()
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Check Out', site_url('checkout'));

        $data['title'] = 'Check Out';
        $data['profile'] = 'Kamar Terpakai';
        $data['user'] = $this->user;
        $data['users'] = $this->M_user->get()->num_rows();
        if ($this->input->post('submit')) {
            $keyword = $this->input->post('keyword');
            $data['kamar'] = $this->M_kamar->getByKeyword($keyword, 2)->result_array();
        } else {
            $data['kamar'] = $this->M_kamar->getKamar(2)->result_array();
        }
        $this->template->load('template', 'transaksi/checkout', $data);

        if ($_POST) {
        }
    }

    public function checkout($id)
    {

        $get = $this->M_transaksi->getBy(['invoice' => $id])->row_array();

        $a_pendapatan = [
            'tanggal' => date('Y-m-d'),
            'pemasukan' => $get['total'],
            'iduser' => $this->users['iduser'],
            'time' => time()
        ];

        $this->M_kamar->beginTrans();
        $this->M_kamar->update(['idstatus' => 3], $get['idkamar']);
        $this->M_pendapatan->insert($a_pendapatan);
        $ok = $this->M_kamar->statusTrans();
        $this->M_kamar->commitTrans($ok);

        $ok && $ok ? setMessage('Berhasil melakukan checkout', 'success') : setMessage('Gagal melakukan checkout', 'danger');
        redirect('transaksi/checkout');
    }
}
