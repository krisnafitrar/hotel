<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Riwayat extends MY_Controller
{
    function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_tamu', 'tamu');
        $this->load->model('M_kamar', 'kamar');
        $this->load->model('M_kelaskamar', 'kelaskamar');
        $this->load->model('M_statuskamar', 'status');

        //set default
        $this->title = 'Transaksi';
        $this->menu = 'transaksi';
        $this->parent = 'transaksi';
        $this->pager = true;
        $this->setKolom();
    }

    public function setKolom()
    {
        $a_tamu = $this->tamu->getListCombo();
        $a_kamar = $this->kamar->getListCombo();

        $a_kolom = [];
        $a_kolom[] = ['kolom' => 'invoice', 'label' => 'Invoice'];
        $a_kolom[] = ['kolom' => 'idkamar', 'label' => 'Kamar', 'type' => 'S', 'option' => $a_kamar];
        $a_kolom[] = ['kolom' => 'tglcheckin', 'label' => 'Checkin'];
        $a_kolom[] = ['kolom' => 'tglcheckout', 'label' => 'Checkout'];
        $a_kolom[] = ['kolom' => 'durasi', 'label' => 'Durasi/hari'];
        $a_kolom[] = ['kolom' => 'idtamu', 'label' => 'Tamu', 'type' => 'S', 'option' => $a_tamu];
        $a_kolom[] = ['kolom' => 'total', 'label' => 'Total', 'set_currency' => true];
        $a_kolom[] = ['kolom' => 'deposit', 'label' => 'Deposit', 'set_currency' => true];


        $this->a_kolom = $a_kolom;
    }
}
