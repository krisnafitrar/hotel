<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Status extends MY_Controller
{


    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('username')) {
            redirect('auth');
        }

        $this->load->model('M_kelaskamar', 'kelaskamar');
        $this->load->model('M_kamar', 'kamar');

        $this->user = $this->M_user->getBy(['username' => $this->session->userdata['username']])->row_array();
    }

    public function index($id = null, $status = null)
    {
        $this->breadcrumb->append_crumb('<i class="fa fa-home"></i> Beranda', site_url());
        $this->breadcrumb->append_crumb('Check In/Out', site_url('transaksi'));
        $this->breadcrumb->append_crumb('Status Kamar', site_url('#'));

        $data['title'] = 'Status Kamar';
        $data['profile'] = 'My Profile';
        $data['user'] = $this->user;
        $data['users'] = $this->M_user->get()->num_rows();

        $data['active'] = 'all';
        $data['a_data'] = $this->kamar->getReference(null, $status)->result_array();
        $data['a_tersedia'] = $this->kamar->getStatus(null, 1)->num_rows();
        $data['a_terpakai'] = $this->kamar->getStatus(null, 2)->num_rows();
        $data['a_kotor'] = $this->kamar->getStatus(null, 3)->num_rows();
        if (!empty($id) && $id != 'all' && empty($status)) {
            $data['active'] = $id;
            $data['a_data'] = $this->kamar->getReference($id)->result_array();
            $data['a_tersedia'] = $this->kamar->getStatus($id, 1)->num_rows();
            $data['a_terpakai'] = $this->kamar->getStatus($id, 2)->num_rows();
            $data['a_kotor'] = $this->kamar->getStatus($id, 3)->num_rows();
        } else {
            if (!empty($id) && !empty($status)) {
                $data['active'] = $id;
                $data['a_data'] = $this->kamar->getReference($id, $status)->result_array();
                $data['a_tersedia'] = $this->kamar->getStatus($id, 1)->num_rows();
                $data['a_terpakai'] = $this->kamar->getStatus($id, 2)->num_rows();
                $data['a_kotor'] = $this->kamar->getStatus($id, 3)->num_rows();
            }
        }

        $data['idkelaskamar'] = $id;
        $data['a_nav'] = $this->kelaskamar->get()->result_array();
        $this->template->load('template', 'transaksi/status', $data);
    }
}
