<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_kamar extends MY_Model
{
    protected $table = 'kamar';
    protected $schema = '';
    public $key = 'idkamar';
    public $value = 'namakamar';

    public function __construct()
    {
        parent::__construct();
    }

    public function getKamar($id)
    {
        if ($id == 2) {
            $query = "SELECT * FROM transaksi trx JOIN kamar ka USING(idkamar) JOIN kelaskamar kk ON ka.idkelaskamar=kk.idkelaskamar JOIN tamu tm USING(idtamu) WHERE ka.idstatus=$id";
        } else {
            $query = "SELECT * FROM kamar JOIN kelaskamar USING(idkelaskamar) WHERE idstatus=$id";
        }
        return $this->db->query($query);
    }

    public function getByKeyword($params, $id)
    {
        if ($id == 2) {
            $query = "SELECT * FROM transaksi trx JOIN kamar ka USING(idkamar) JOIN kelaskamar kk ON ka.idkelaskamar=kk.idkelaskamar JOIN tamu tm USING(idtamu) WHERE ka.namakamar LIKE '%$params%' OR tm.namatamu LIKE '%$params%' AND ka.idstatus=$id";
        } else {
            $query = "SELECT * FROM kamar JOIN kelaskamar USING(idkelaskamar) WHERE namakamar LIKE'%$params%' AND idstatus=$id";
        }
        return $this->db->query($query);
    }

    public function getReference($id = null, $status = null)
    {

        if ($id == 'all' && !empty($status)) {
            $wh = " WHERE idstatus=$status";
        } else if (!empty($id) && $id != 'all' && !empty($status)) {
            $wh = " WHERE idkelaskamar=$id AND idstatus=$status";
        } else if (!empty($id) && empty($status)) {
            $wh = " WHERE idkelaskamar=$id";
        } else {
            $wh = "";
        }


        $query = "SELECT * FROM kamar JOIN kelaskamar USING(idkelaskamar) JOIN statuskamar USING(idstatus)" . $wh;
        return $this->db->query($query);
    }

    public function getStatus($id, $status)
    {
        if ($id == 'all' && !empty($status)) {
            $wh = " WHERE idstatus=$status";
        } else if (!empty($id) && $id != 'all' && !empty($status)) {
            $wh = " WHERE idkelaskamar=$id AND idstatus=$status";
        } else if (!empty($id) && empty($status)) {
            $wh = " WHERE idkelaskamar=$id";
        } else {
            $wh = " WHERE idstatus=$status";
        }

        $query = "SELECT * FROM kamar" . $wh;
        return $this->db->query($query);
    }
}
