<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_kelaskamar extends MY_Model
{
    protected $table = 'kelaskamar';
    protected $schema = '';
    public $key = 'idkelaskamar';
    public $value = 'kelaskamar';
}
