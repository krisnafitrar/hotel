<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_menu extends MY_Model
{
    protected $table = 'user_menu';
    protected $schema = '';
    public $key = 'idmenu';
    public $value = 'menu';

    function __construct()
    {
        parent::__construct();
    }
}
