<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_pegawai extends MY_Model
{
    protected $table = 'pegawai';
    protected $schema = '';
    public $key = 'idpegawai';
    public $value = 'namapegawai';

    public function __construct()
    {
        parent::__construct();
    }
}
