<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_posisi extends MY_Model
{
    protected $table = 'posisi';
    protected $schema = '';
    public $key = 'idposisi';
    public $value = 'posisi';

    public function __construct()
    {
        parent::__construct();
    }
}
