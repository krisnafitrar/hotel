<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_statuskamar extends MY_Model
{
    protected $table = 'statuskamar';
    protected $schema = '';
    public $key = 'idstatus';
    public $value = 'status';
}
