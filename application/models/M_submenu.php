<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_submenu extends MY_Model
{
    protected $table = 'user_submenu';
    protected $schema = '';
    public $key = 'idsubmenu';
    public $value = 'title';

    function __construct()
    {
        parent::__construct();
    }
}
