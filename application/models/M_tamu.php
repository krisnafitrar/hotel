<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_tamu extends MY_Model
{
    protected $table = 'tamu';
    protected $schema = '';
    public $key = 'idtamu';
    public $value = 'namatamu';

    public function __construct()
    {
        parent::__construct();
    }
}
