<?php
defined('BASEPATH') or exit('No direct script access allowed');

class M_transaksi extends MY_Model
{
    protected $table = 'transaksi';
    protected $schema = '';
    public $key = 'invoice';
    public $value = '';

    public function __construct()
    {
        parent::__construct();
    }
}
