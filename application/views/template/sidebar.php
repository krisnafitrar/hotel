<nav class="col-md-2 d-none d-md-block bg-light sidebar">
    <div class="sidebar-sticky">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link active" href="#">
                    <span data-feather="home"></span>
                    Dashboard <span class="sr-only">(current)</span>
                </a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Master</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="<?= base_url('master/users') ?>">User</a>
                    <a class="dropdown-item" href="<?= base_url('master/siswa') ?>">Siswa</a>
                    <a class="dropdown-item" href="<?= base_url('master/guru') ?>">Guru</a>
                    <a class="dropdown-item" href="<?= base_url('master/kelas') ?>">Kelas</a>
                    <a class="dropdown-item" href="<?= base_url('master/tahunakademik') ?>">Tahun Akademik</a>
                    <a class="dropdown-item" href="<?= base_url('master/jenispelanggaran') ?>">Jenis Pelanggaran</a>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= base_url('home/logout') ?>">Logout</a>
            </li>
        </ul>
    </div>
</nav>