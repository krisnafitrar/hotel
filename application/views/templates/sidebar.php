<!-- Sidebar -->
<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <?php
    $setting = settingSIM();

    ?>
    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?= site_url() ?>">
        <div class="sidebar-brand-icon">
            <i class="<?= $setting['icon_app'] ?>"></i>
        </div>
        <div class="sidebar-brand-text mx-2 mt-2"><?= $setting['app_name'] ?></div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider">

    <li class="nav-item <?= $title == "Dashboard" ? 'active' : '' ?>">
        <a class="nav-link" href="<?= site_url() ?>">
            <i class="fas fa-home"></i>
            <span>Dashboard</span>
        </a>
    </li>

    <!-- Query menu -->
    <?php
    $role_id = $this->session->userdata('role_id');
    $qm = "SELECT * FROM user_access_menu JOIN user_menu USING(idmenu) WHERE role_id=" . $role_id;
    $menu = $this->db->query($qm)->result_array();
    ?>

    <!-- LOOPING MENU -->
    <?php foreach ($menu as $m) : ?>
        <!-- Nav Item - Pages Collapse Menu -->
        <?php
        $qsm = "SELECT * FROM user_submenu WHERE idmenu=" . $m['idmenu'];
        $submenu = $this->db->query($qsm)->result_array();
        ?>

        <li class="nav-item">
            <a class="nav-link" href="#" data-toggle="collapse" data-target="#menu<?= $m['idmenu'] ?>" aria-expanded="true" aria-controls="collapseMenu<?= $m['idmenu'] ?>">
                <i class="<?= $m['icon']; ?>"></i>
                <span><?= $m['menu']; ?></span>
            </a>
            <div id="menu<?= $m['idmenu'] ?>" class="collapse <?= $active == $m['idmenu'] ? 'show' : '' ?>" aria-labelledby="headingPages" data-parent="#accordionSidebar">
                <div class="bg-white py-3 collapse-inner rounded">
                    <!-- SIAPKAN SUBMENU MENU -->
                    <?php foreach ($submenu as $sm) { ?>
                        <!-- Nav Item - Dashboard -->
                        <a class="collapse-item <?= $title == $sm['submenu'] ? 'active' : '' ?>" href="<?= base_url($sm['url']); ?>"><?= $sm['submenu'] ?></a>
                    <?php } ?>
                </div>
            </div>

            <!-- Divider -->
        </li>
    <?php endforeach ?>

    <!-- Nav Item - Charts -->
    <li class="nav-item">
        <a class="nav-link" href="#" data-toggle="modal" data-target="#logoutModal" data-toggle="modal" data-target="#logoutModal">
            <i class="fas fa-sign-out-alt"></i>
            <span>Logout</span></a>
    </li>

    <!-- Divider -->
    <hr class="sidebar-divider d-none d-md-block">

    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

</ul>
<!-- End of Sidebar -->