            <!-- Main Content -->
            <div id="content">
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800"><?= $profile; ?></h1>

                    <div class="row">
                        <div class="col-lg-6">
                            <?= $this->session->flashdata('message'); ?>
                        </div>
                    </div>

                    <div class="card mb-3">
                        <div class="row no-gutters">
                            <div class="col-md-8 mt-3 ml-4">
                                <form action="<?= base_url('transaksi/checkout') ?>" method="POST">
                                    <div class="input-group">
                                        <input type="text" name="keyword" class="form-control" placeholder="Cari kamar yang digunakan ..." aria-label="Cari guru ..." aria-describedby="button-addon4" autocomplete="off" autofocus>
                                        <div class="input-group-append" id="button-addon4">
                                            <input class="btn btn-success" type="submit" name="submit">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <div class="card-body">
                                    <div class="row">
                                        <?php
                                        $cols = 3;
                                        $x = 0;
                                        $width = 12 / $cols;
                                        $count = count($kamar);
                                        ?>

                                        <?php if (count($kamar) > 0) : ?>
                                            <?php foreach ($kamar as $k) : ?>
                                                <div class="col-md-4 col-lg-<?= $width ?> ftco-animate">
                                                    <div class="card" style="width: 18rem;">
                                                        <img src="<?= base_url('assets/img/kamar/') . $k['foto'] ?>" class="card-img-top" alt="">
                                                        <div class="card-body">
                                                            <h5 class="card-title"><b><?= $k['namakamar'] ?></b></h5>
                                                            <div class="mb-3">
                                                                <table>
                                                                    <tr>
                                                                        <td><i class="fa fa-user-tie"></i> Tamu : <?= $k['namatamu'] ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><i class="fa fa-bed"></i> Tipe : <?= $k['kelaskamar'] ?></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <button type="button" data-type="btn" data-id="<?= $k['invoice'] ?>" class="btn btn-warning btn-block">Check Out <i class="fa fa-external-link-square-alt ml-1"></i></button>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                $x++;
                                                if ($x % $cols == 0 && $x < $count) {
                                                    echo '</div><div class="row">';
                                                }
                                                ?>
                                            <?php endforeach ?>
                                        <?php else : ?>
                                            <div class="col-md-12 mt-3">
                                                <div class="alert alert-warning" role="alert">
                                                    <h4 class="alert-heading"><b>Kamar tidak ditemukan!</b></h4>
                                                    <p>Kamar yang anda cari tidak ditemukan atau tidak tersedia.</p>
                                                    <hr>
                                                    <p class="mb-0"><b>Bonero Hotel. <?= date('Y') ?></b></p>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->
            <!-- Logout Modal-->
            <div class="modal fade" id="checkoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Check Out</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">Anda yakin akan checkout?</div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Batal</button>
                            <button class="btn btn-warning" type="button" data-id="" data-type="btn-checkout"><i class="fas fa-sign-out-alt"></i> Checkout</but>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                $('[data-type=btn]').click(function() {
                    var id = $(this).attr('data-id');
                    var modal = $('#checkoutModal');
                    modal.find('[data-type=btn-checkout]').attr('data-id', id);
                    modal.modal();
                });

                $('[data-type=btn-checkout]').click(function() {
                    var id = $(this).attr('data-id');
                    location.href = '<?= base_url('transaksi/checkout/checkout/') ?>' + id;
                });
            </script>