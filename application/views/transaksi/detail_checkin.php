<?php

date_default_timezone_set("Asia/Bangkok");
?>
<div class="content">
    <div class="container-fluid">
        <!-- Page Heading -->
        <h1 class="h3 mb-4 text-gray-800"><?= $profile; ?></h1>
        <div class="card mb-3">
            <div class="row no-gutters">
                <div class="col-md-12">
                    <div class="card-body">
                        <?= $this->session->flashdata('message'); ?>
                        <form action="<?= base_url('transaksi/checkin') ?>" method="POST">
                            <div class="row">
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="checkin">Tanggal Check In</label>
                                        <input type="text" class="form-control" name="checkin" id="checkin" value="<?= date('Y-m-d') ?>" readonly>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="waktu_checkin">Waktu Check In</label>
                                        <input type="text" class="form-control" name="waktu_checkin" id="waktu_checkin" value="<?= date('H:i') ?>" readonly>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="checkout">Tanggal Check Out</label>
                                        <input type="date" class="form-control" name="checkout" id="checkout" required>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="waktu_checkout">Waktu Check Out</label>
                                        <input type="text" class="form-control" name="waktu_checkout" id="waktu_checkout" value="12:00" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="tamu">Nama Tamu</label>
                                    <select name="tamu" id="tamu" class="form-control" required>
                                    </select>
                                </div>
                            </div>
                            <div class="row mt-3">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="total">Harga</label>
                                        <input type="text" name="total" id="total" class="form-control" value="<?= $kamar['harga'] ?>" readonly>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-2">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="deposit">Deposit</label>
                                        <input type="number" name="deposit" id="deposit" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="idkamar" value="<?= $kamar['idkamar'] ?>">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <a href="<?= base_url('transaksi/checkin') ?>" class="btn btn-sm btn-secondary">Kembali</a>
                                        <button type="submit" class="btn btn-sm btn-success">Check In</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('#tamu').select2({
        minimumInputLength: 3,
        allowClear: true,
        placeholder: 'Masukkan nama tamu',
        ajax: {
            dataType: 'json',
            type: 'POST',
            url: '<?= site_url('transaksi/checkin/cariTamu') ?>',
            delay: 250,
            data: function(params) {
                return {
                    cari: params.term
                }
            },
            processResults: function(data, page) {
                return {
                    results: data
                };
            },
        }
    });
</script>