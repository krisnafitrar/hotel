            <!-- Main Content -->
            <div id="content">
                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-4 text-gray-800"><?= $profile; ?></h1>

                    <div class="row">
                        <div class="col-lg-6">
                            <?= $this->session->flashdata('message'); ?>
                        </div>
                    </div>

                    <div class="card mb-3">
                        <div class="row no-gutters">
                            <div class="col-md-8 mt-3 ml-4">
                                <form action="<?= base_url('transaksi/checkin') ?>" method="POST">
                                    <div class="input-group">
                                        <input type="text" name="keyword" class="form-control" placeholder="Cari kamar yang tersedia ..." aria-label="Cari guru ..." aria-describedby="button-addon4" autocomplete="off" autofocus>
                                        <div class="input-group-append" id="button-addon4">
                                            <input class="btn btn-success" type="submit" name="submit">
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-md-12">
                                <div class="card-body">
                                    <div class="row">
                                        <?php
                                        $cols = 3;
                                        $x = 0;
                                        $width = 12 / $cols;
                                        $count = count($kamar);
                                        ?>

                                        <?php if (count($kamar) > 0) : ?>
                                            <?php foreach ($kamar as $k) : ?>
                                                <div class="col-md-4 col-lg-<?= $width ?> ftco-animate">
                                                    <div class="card" style="width: 18rem;">
                                                        <img src="<?= base_url('assets/img/kamar/') . $k['foto'] ?>" class="card-img-top" alt="">
                                                        <div class="card-body">
                                                            <h5 class="card-title"><b><?= $k['namakamar'] ?></b></h5>
                                                            <div class="mb-3">
                                                                <table>
                                                                    <tr>
                                                                        <td><i class="fa fa-bed"></i> Tipe : <?= $k['kelaskamar'] ?></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td><i class="fa fa-user-friends"></i> Max : <?= $k['kapasitas'] ?> Org</td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                            <a href="<?= base_url('transaksi/checkin/detail/' . $k['idkamar']) ?>" class="btn btn-info btn-block">Pilih Kamar <i class="fa fa-external-link-square-alt ml-1"></i></a>
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                $x++;
                                                if ($x % $cols == 0 && $x < $count) {
                                                    echo '</div><div class="row">';
                                                }
                                                ?>
                                            <?php endforeach ?>
                                        <?php else : ?>
                                            <div class="col-md-12 mt-3">
                                                <div class="alert alert-warning" role="alert">
                                                    <h4 class="alert-heading"><b>Kamar tidak ditemukan!</b></h4>
                                                    <p>Mungkin kamar yang kamu cari sedang digunakan oleh tamu atau memang kamar tidak tersedia.</p>
                                                    <hr>
                                                    <p class="mb-0"><b>Bonero Hotel. <?= date('Y') ?></b></p>
                                                </div>
                                            </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->