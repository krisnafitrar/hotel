<!-- Main Content -->
<div id="content">
    <!-- Begin Page Content -->
    <div class="container-fluid">

        <div class="row">
            <div class="col-lg-6">
                <?= $this->session->flashdata('message'); ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">
                        <h2><?= $title ?></h2>
                        <ul class="nav nav-pills mt-3">
                            <li class="nav-item">
                                <a class="nav-link <?= $active == 'all' ? 'active' : '' ?>" href="<?= base_url('transaksi/status') ?>">Semua Tipe</a>
                            </li>
                            <?php foreach ($a_nav as $nav) : ?>
                                <li class="nav-item">
                                    <a class="nav-link <?= $active == $nav['idkelaskamar'] ? 'active' : '' ?>" href="<?= base_url('transaksi/status/index/' . $nav['idkelaskamar']) ?>"><?= $nav['kelaskamar'] ?></a>
                                </li>
                            <?php endforeach; ?>
                        </ul>

                        <div class="col-md-6">
                            <table class="mt-3 table">
                                <tr>
                                    <?php
                                    $a_link = $idkelaskamar == null ? 'all' : $idkelaskamar;
                                    ?>
                                    <td width="0px"><a href="<?= base_url('transaksi/status/index/' . $a_link . '/1') ?>" class="badge badge-success">Tersedia<b> : <?= $a_tersedia ?></b></a></td>
                                    <td width="0px"><a href="<?= base_url('transaksi/status/index/' . $a_link . '/2') ?>" class="badge badge-danger">Terpakai</a><b> : <?= $a_terpakai ?></b></td>
                                    <td width="0px"><a href="<?= base_url('transaksi/status/index/' . $a_link . '/3') ?>" class="badge badge-warning">Kotor</a><b> : <?= $a_kotor ?></b></td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col">#</th>
                                        <th scope="col">Nama Kamar</th>
                                        <th scope="col">Tipe Kamar</th>
                                        <th scope="col">Kapasitas</th>
                                        <th scope="col">Harga</th>
                                        <th scope="col">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php $i = 1; ?>
                                    <?php if (!empty($a_data)) : ?>
                                        <?php foreach ($a_data as $data) : ?>
                                            <tr>
                                                <th scope="row"><?= $i++ ?></th>
                                                <td><?= $data['namakamar'] ?></td>
                                                <td><?= $data['kelaskamar'] ?></td>
                                                <td><?= $data['kapasitas'] . ' Dewasa' ?></td>
                                                <td><?= toRupiah($data['harga']) ?></td>
                                                <td><span class="badge badge-<?= $data['color'] ?>"><?= $data['status'] ?></span></td>
                                            </tr>
                                        <?php endforeach; ?>
                                    <?php else : ?>
                                        <tr class="table table-danger">
                                            <td colspan="6" class="text-dark text-center">Kamar tidak ditemukan</td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- /.container-fluid -->
    </div>
    <!-- End of Main Content -->
</div>