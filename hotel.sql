-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 25 Mar 2020 pada 05.30
-- Versi server: 10.4.11-MariaDB
-- Versi PHP: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hotel`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `kamar`
--

CREATE TABLE `kamar` (
  `idkamar` varchar(128) NOT NULL,
  `idkelaskamar` int(11) DEFAULT NULL,
  `namakamar` varchar(128) DEFAULT NULL,
  `kapasitas` int(11) DEFAULT NULL,
  `harga` int(11) DEFAULT NULL,
  `fasilitas` text DEFAULT NULL,
  `foto` varchar(128) DEFAULT NULL,
  `deskripsi` text DEFAULT NULL,
  `info_tambahan` text DEFAULT NULL,
  `idstatus` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kamar`
--

INSERT INTO `kamar` (`idkamar`, `idkelaskamar`, `namakamar`, `kapasitas`, `harga`, `fasilitas`, `foto`, `deskripsi`, `info_tambahan`, `idstatus`) VALUES
('K001', 1, 'Deluxe Room No.01', 2, 2000000, 'Wifi,Kamar mandi dalam,AC', 'furniture-998265_1920.jpg', 'N/A', 'N/A', 2),
('K002', 1, 'Deluxe Room No.02', 2, 1500000, 'AC, KM dalam, TV, Wifi', 'bed2.jpg', 'Ini kamar kelas atas', 'N/A', 1),
('K003', 2, 'Superior Room No.1', 2, 565000, 'AC, TV', 'bed1.jpg', 'ini kamar tipe superior', 'N/A', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `kelaskamar`
--

CREATE TABLE `kelaskamar` (
  `idkelaskamar` int(11) NOT NULL,
  `kelaskamar` varchar(128) DEFAULT NULL,
  `info_tambahan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kelaskamar`
--

INSERT INTO `kelaskamar` (`idkelaskamar`, `kelaskamar`, `info_tambahan`) VALUES
(1, 'Deluxe Room', 'Ini adalah jenis deluxe room'),
(2, 'Superior room', 'Ini adalah kamar superior'),
(3, 'Standart Room', 'Ini adalah kamar tipe standar');

-- --------------------------------------------------------

--
-- Struktur dari tabel `pegawai`
--

CREATE TABLE `pegawai` (
  `idpegawai` int(11) NOT NULL,
  `namapegawai` varchar(128) DEFAULT NULL,
  `jeniskelamin` varchar(128) DEFAULT NULL,
  `noidentitas` varchar(128) DEFAULT NULL,
  `notelp` varchar(15) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `idposisi` int(11) DEFAULT NULL,
  `foto` varchar(128) DEFAULT NULL,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pegawai`
--

INSERT INTO `pegawai` (`idpegawai`, `namapegawai`, `jeniskelamin`, `noidentitas`, `notelp`, `alamat`, `idposisi`, `foto`, `status`) VALUES
(1, 'Budi Sudarsono', 'L', '100020', '0812345', 'JL.Rungkut Surabaya', 1, 'slider.jpg', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `pendapatan`
--

CREATE TABLE `pendapatan` (
  `idpendapatan` int(11) NOT NULL,
  `tgl` varchar(128) DEFAULT NULL,
  `pendapatan` int(11) DEFAULT NULL,
  `iduser` int(11) DEFAULT NULL,
  `time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Struktur dari tabel `pengaturan`
--

CREATE TABLE `pengaturan` (
  `idpengaturan` varchar(128) NOT NULL,
  `namapengaturan` varchar(128) DEFAULT NULL,
  `value` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `pengaturan`
--

INSERT INTO `pengaturan` (`idpengaturan`, `namapengaturan`, `value`) VALUES
('app_name', 'Pengaturan Nama Aplikasi', 'Hotel Bonero'),
('icon_app', 'Icon Aplikasi', 'fas fa-hotel');

-- --------------------------------------------------------

--
-- Struktur dari tabel `posisi`
--

CREATE TABLE `posisi` (
  `idposisi` int(11) NOT NULL,
  `posisi` varchar(128) DEFAULT NULL,
  `info_tambahan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `posisi`
--

INSERT INTO `posisi` (`idposisi`, `posisi`, `info_tambahan`) VALUES
(1, 'HRD', 'Posisi HRD');

-- --------------------------------------------------------

--
-- Struktur dari tabel `statuskamar`
--

CREATE TABLE `statuskamar` (
  `idstatus` int(11) NOT NULL,
  `status` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `statuskamar`
--

INSERT INTO `statuskamar` (`idstatus`, `status`) VALUES
(1, 'Tersedia'),
(2, 'Terpakai'),
(3, 'Kotor');

-- --------------------------------------------------------

--
-- Struktur dari tabel `tamu`
--

CREATE TABLE `tamu` (
  `idtamu` int(11) NOT NULL,
  `namatamu` varchar(128) DEFAULT NULL,
  `jeniskelamin` varchar(128) DEFAULT NULL,
  `notelp` varchar(128) DEFAULT NULL,
  `alamat` text DEFAULT NULL,
  `info_tambahan` text DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `tamu`
--

INSERT INTO `tamu` (`idtamu`, `namatamu`, `jeniskelamin`, `notelp`, `alamat`, `info_tambahan`) VALUES
(1, 'Umum', 'L', NULL, 'N/A', 'N/A');

-- --------------------------------------------------------

--
-- Struktur dari tabel `transaksi`
--

CREATE TABLE `transaksi` (
  `idbooking` varchar(128) NOT NULL,
  `tglcheckin` varchar(128) DEFAULT NULL,
  `waktucheckin` int(11) DEFAULT NULL,
  `tglcheckout` varchar(128) DEFAULT NULL,
  `waktucheckout` int(11) DEFAULT NULL,
  `durasi` int(11) DEFAULT NULL,
  `idtamu` int(11) DEFAULT NULL,
  `idkamar` varchar(11) DEFAULT NULL,
  `total` int(11) DEFAULT NULL,
  `deposit` int(11) DEFAULT NULL,
  `denda` int(11) DEFAULT NULL,
  `time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `transaksi`
--

INSERT INTO `transaksi` (`idbooking`, `tglcheckin`, `waktucheckin`, `tglcheckout`, `waktucheckout`, `durasi`, `idtamu`, `idkamar`, `total`, `deposit`, `denda`, `time`) VALUES
('BK001', '2020-03-25', NULL, '2020-03-26', NULL, 1, 1, 'K001', 2000000, 2000000, NULL, 12323423);

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `iduser` int(11) NOT NULL,
  `nama` varchar(128) DEFAULT NULL,
  `username` varchar(128) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL,
  `create_at` int(11) DEFAULT NULL,
  `is_aktif` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`iduser`, `nama`, `username`, `email`, `password`, `role_id`, `create_at`, `is_aktif`) VALUES
(1, 'Admin Hotel', 'admin', 'admin@gmail.com', '$2y$10$GzAdOn.DT6mClXlnKm.YDuR8X3QlbvQNtu9PwYx08y.Hz2GIlhUFW', 1, NULL, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `idaccess` int(11) NOT NULL,
  `idmenu` int(11) DEFAULT NULL,
  `role_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_access_menu`
--

INSERT INTO `user_access_menu` (`idaccess`, `idmenu`, `role_id`) VALUES
(1, 1, 1),
(2, 2, 1),
(3, 3, 1),
(4, 4, 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_menu`
--

CREATE TABLE `user_menu` (
  `idmenu` int(11) NOT NULL,
  `menu` varchar(128) DEFAULT NULL,
  `icon` varchar(128) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_menu`
--

INSERT INTO `user_menu` (`idmenu`, `menu`, `icon`, `is_active`) VALUES
(1, 'Master', 'fas fa-fw fa-database', 1),
(2, 'Check in / out', 'fas fa-fw fa-key', 1),
(3, 'Pengaturan', 'fas fa-fw fa-cog', 1),
(4, 'Laporan', 'fas fa-fw fa-book', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_role`
--

CREATE TABLE `user_role` (
  `role_id` int(11) NOT NULL,
  `role` varchar(128) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_role`
--

INSERT INTO `user_role` (`role_id`, `role`) VALUES
(1, 'Superadmin'),
(2, 'Admin');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_submenu`
--

CREATE TABLE `user_submenu` (
  `idsubmenu` int(11) NOT NULL,
  `submenu` varchar(128) DEFAULT NULL,
  `idmenu` int(11) DEFAULT NULL,
  `url` varchar(128) DEFAULT NULL,
  `is_active` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user_submenu`
--

INSERT INTO `user_submenu` (`idsubmenu`, `submenu`, `idmenu`, `url`, `is_active`) VALUES
(1, 'Data Kamar', 1, 'masters/kamar', 1),
(2, 'Data Kelas Kamar', 1, 'masters/kelaskamar', 1),
(3, 'Data Pegawai', 1, 'masters/pegawai', 1),
(4, 'Data Tamu', 1, 'masters/tamu', 1),
(5, 'Check In', 2, 'transaksi/checkin', 1),
(6, 'Pengaturan Aplikasi', 3, 'pengaturan/setting', 1),
(8, 'Laporan Pendapatan', 4, 'laporan/pendapatan', 1),
(9, 'List Posisi', 1, 'masters/posisi', 1),
(10, 'Role', 3, 'pengaturan/role', 1),
(11, 'Pengaturan Menu', 3, 'pengaturan/menu', 1),
(12, 'Pengaturan Submenu', 3, 'pengaturan/submenu', 1),
(13, 'Data User', 1, 'masters/user', 1),
(14, 'Check Out', 2, 'transaksi/checkout', 1);

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `kamar`
--
ALTER TABLE `kamar`
  ADD PRIMARY KEY (`idkamar`),
  ADD KEY `idkelaskamar` (`idkelaskamar`),
  ADD KEY `idstatus` (`idstatus`);

--
-- Indeks untuk tabel `kelaskamar`
--
ALTER TABLE `kelaskamar`
  ADD PRIMARY KEY (`idkelaskamar`);

--
-- Indeks untuk tabel `pegawai`
--
ALTER TABLE `pegawai`
  ADD PRIMARY KEY (`idpegawai`),
  ADD KEY `idposisi` (`idposisi`);

--
-- Indeks untuk tabel `pendapatan`
--
ALTER TABLE `pendapatan`
  ADD PRIMARY KEY (`idpendapatan`),
  ADD KEY `iduser` (`iduser`);

--
-- Indeks untuk tabel `pengaturan`
--
ALTER TABLE `pengaturan`
  ADD PRIMARY KEY (`idpengaturan`);

--
-- Indeks untuk tabel `posisi`
--
ALTER TABLE `posisi`
  ADD PRIMARY KEY (`idposisi`);

--
-- Indeks untuk tabel `statuskamar`
--
ALTER TABLE `statuskamar`
  ADD PRIMARY KEY (`idstatus`);

--
-- Indeks untuk tabel `tamu`
--
ALTER TABLE `tamu`
  ADD PRIMARY KEY (`idtamu`);

--
-- Indeks untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD PRIMARY KEY (`idbooking`),
  ADD KEY `idkamar` (`idkamar`),
  ADD KEY `idpelanggan` (`idtamu`);

--
-- Indeks untuk tabel `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`iduser`),
  ADD KEY `role_id` (`role_id`);

--
-- Indeks untuk tabel `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`idaccess`),
  ADD KEY `idmenu` (`idmenu`),
  ADD KEY `role_id` (`role_id`);

--
-- Indeks untuk tabel `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`idmenu`);

--
-- Indeks untuk tabel `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`role_id`);

--
-- Indeks untuk tabel `user_submenu`
--
ALTER TABLE `user_submenu`
  ADD PRIMARY KEY (`idsubmenu`),
  ADD KEY `idmenu` (`idmenu`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `kelaskamar`
--
ALTER TABLE `kelaskamar`
  MODIFY `idkelaskamar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `pegawai`
--
ALTER TABLE `pegawai`
  MODIFY `idpegawai` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `pendapatan`
--
ALTER TABLE `pendapatan`
  MODIFY `idpendapatan` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT untuk tabel `posisi`
--
ALTER TABLE `posisi`
  MODIFY `idposisi` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `statuskamar`
--
ALTER TABLE `statuskamar`
  MODIFY `idstatus` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT untuk tabel `tamu`
--
ALTER TABLE `tamu`
  MODIFY `idtamu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `users`
--
ALTER TABLE `users`
  MODIFY `iduser` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT untuk tabel `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `idaccess` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `idmenu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `user_role`
--
ALTER TABLE `user_role`
  MODIFY `role_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `user_submenu`
--
ALTER TABLE `user_submenu`
  MODIFY `idsubmenu` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- Ketidakleluasaan untuk tabel pelimpahan (Dumped Tables)
--

--
-- Ketidakleluasaan untuk tabel `kamar`
--
ALTER TABLE `kamar`
  ADD CONSTRAINT `kamar_ibfk_1` FOREIGN KEY (`idkelaskamar`) REFERENCES `kelaskamar` (`idkelaskamar`),
  ADD CONSTRAINT `kamar_ibfk_2` FOREIGN KEY (`idstatus`) REFERENCES `statuskamar` (`idstatus`);

--
-- Ketidakleluasaan untuk tabel `pegawai`
--
ALTER TABLE `pegawai`
  ADD CONSTRAINT `pegawai_ibfk_1` FOREIGN KEY (`idposisi`) REFERENCES `posisi` (`idposisi`);

--
-- Ketidakleluasaan untuk tabel `transaksi`
--
ALTER TABLE `transaksi`
  ADD CONSTRAINT `transaksi_ibfk_2` FOREIGN KEY (`idkamar`) REFERENCES `kamar` (`idkamar`),
  ADD CONSTRAINT `transaksi_ibfk_3` FOREIGN KEY (`idtamu`) REFERENCES `tamu` (`idtamu`);

--
-- Ketidakleluasaan untuk tabel `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `user_role` (`role_id`);

--
-- Ketidakleluasaan untuk tabel `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD CONSTRAINT `user_access_menu_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `user_role` (`role_id`),
  ADD CONSTRAINT `user_access_menu_ibfk_3` FOREIGN KEY (`idmenu`) REFERENCES `user_menu` (`idmenu`);

--
-- Ketidakleluasaan untuk tabel `user_submenu`
--
ALTER TABLE `user_submenu`
  ADD CONSTRAINT `user_submenu_ibfk_1` FOREIGN KEY (`idmenu`) REFERENCES `user_menu` (`idmenu`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
